# Praxisprojekt "Networks of Power" 

This project builds a basis to continue working with the data extracted from various documents concerning the Portuguese empire between 1642 and 1822. The goal was to create dictionaries out of the entities recorded in a csv file. 

To do so, the entities have to be disambiguated first. I decided to work with pandas dataframes and a python library called *String Grouper*, developed by Chris van den Berg. The package with instructions and examples can be found on GitHub [here](https://github.com/Bergvca/string_grouper).

## Cleaning the Data
In order to effectively use _string grouper_, some cells with multiple values in them had to be cleaned of any unwanted characters such as brackets and split by comma first, so each cell contains only one value. The panda function `.explode()` helped with that, but the columns have to be exploded separately to avoid a value error due to an uneven distribution of values in cells across the dataframe.
Exploding the columns separately avoids any value errors, but it does lead to potential other problems as it can turn cells originally looking like this:
| doc_id | sender | recipient |
| ------ | ------ | ---       |
| 281830 | ['Manoel Nunes', 'João Gomes Teixeira']|['D. João', 'D. Maria I']        |

into this: 
| doc_id | sender | recipient |
| ------ | ------ | ---       |
| 281830 | Manoel Nunes| D. João|
| 281830 | Manoel Nunes| D. Maria I|
| 281830 |João Gomes Teixeira| D. João|
| 281830 |João Gomes Teixeira| D. Maria I|

It seems to be a relatively rare case however and all the necessary information is still attached (like the document ID).

## Splitting the Data into Managable Chunks
Since the dataframe is fairly large, working with all of it at once can lead to out of memory issues. To avoid this, I broke the original dataframe up into multiple smaller ones with the year the document was sent being the deciding factor. The time periods chosen are the reigning years of the Portuguese monarchs. 

## Deduplication of Senders
### Creating Unique IDs
The _string grouper_ works by matching strings in one dataframe against strings in either the same or in another dataframe and comparing their similarity. Strings above a certain treshhold of similarity, either the default of 0.8 or any value determined by the user, are given as output of the matcher. 

To have one dataframe with a consistent, determining version of a name that the matcher can compare against, the `group_similar_strings()` function available in _String Grouper_ is used. This function goes over the _sender_ column of the dataframe and groups similar names together under one representative for that name group. This representative then becomes the definitive version of this name that the `match_strings()` function can match against.

To give each name a unique identifier, I created series of numbers, mapped them to the names and set the new name ID as the index.

### Comparing Senders
Next, the string matcher compares the master string on the left side with the duplicate string on the right side. Each name on the right side is unique and identifiable by the name_id assigned to it previously, while the name on the left side is the name as it currently is in the dataframe, its position identifiable by the doc_id.   
The column *"similarity"* in the middle measures how similar the right and the left string are to each other. The higher the number, the higher the possibility that the left side is a variation of the name on the right side. 0.74 was chosen as the minimum similarity because it seemed to yield the most accurate result, but mistakes are still possible. 1.0 means a perfect match. 

To avoid memory errors, I compared the representative names with unique IDs against the senders saved in the variables that were previously split by time period. To create a log, the result of each matching is saved into a new csv file. This way, the process is logged and possible mistakes in the deduplication process can be tracked easily. 

The process is repeated for each time period relevant for the study. 

### Deduplication of Senders
The next step would be to write the changes into the original csv file with all the data, using the representative name to correct name variations. 

Then, probably using the logs, dictionaries can be created, using the representative name as a key and all the found variations of a name as entries for the dictionary. 

## Next Steps
With the dictionaries, the data could eventually be used for Linked Open Data, for example for projects similar to the _Virtual International Authority File (VIAF)_. Sticking with the example of the senders, it would be possible to search for any variation of a name that exists within that name's dictionary and get back the representative version of said name, including all its aliases. This name could then be connected to further information - biography, relations to people and institutions, affiliations etc. 

With the Linked Open Data approach, this information wouldn't just be available for one database either, but could be connected and used in other databases unrelated to this one but connected by the same LOD principles and further research that way. 






